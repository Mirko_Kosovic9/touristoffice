package application;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.text.AbstractDocument.Content;

import java.awt.BorderLayout;
import java.awt.Container;

import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.Font;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JPanel;
import javax.swing.JMenuBar;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class HotelWindow {

	protected static final int Exit_on_close = 0;
	private JFrame frmNeuesHotel;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_6;
	private JTextField textField_7;
	private JTextField textField_8;
	private final JTextField textField_3 = new JTextField();
	private JComboBox comboBox;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					HotelWindow window = new HotelWindow();
					window.frmNeuesHotel.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public HotelWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmNeuesHotel = new JFrame();
		frmNeuesHotel.setFont(new Font("Bookman Old Style", Font.PLAIN, 14));
		frmNeuesHotel.setTitle("Neues Hotel");
		frmNeuesHotel.setBounds(100, 100, 639, 436);
		frmNeuesHotel.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmNeuesHotel.getContentPane().setLayout(null);
		
	
		
		textField = new JTextField();
		textField.setBounds(112, 83, 436, 27);
		frmNeuesHotel.getContentPane().add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(112, 120, 436, 27);
		frmNeuesHotel.getContentPane().add(textField_1);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(112, 157, 121, 27);
		frmNeuesHotel.getContentPane().add(textField_2);
		
		textField_4 = new JTextField();
		textField_4.setColumns(10);
		textField_4.setBounds(112, 231, 121, 27);
		frmNeuesHotel.getContentPane().add(textField_4);
		
		JLabel l_name = new JLabel("Name");
		l_name.setFont(new Font("Verdana", Font.PLAIN, 13));
		l_name.setBounds(22, 92, 63, 21);
		frmNeuesHotel.getContentPane().add(l_name);
		
		JLabel l_name_1 = new JLabel("Adresse");
		l_name_1.setFont(new Font("Verdana", Font.PLAIN, 13));
		l_name_1.setBounds(22, 123, 63, 21);
		frmNeuesHotel.getContentPane().add(l_name_1);
		
		JLabel l_name_1_1 = new JLabel("PLZ");
		l_name_1_1.setFont(new Font("Verdana", Font.PLAIN, 13));
		l_name_1_1.setBounds(22, 160, 63, 21);
		frmNeuesHotel.getContentPane().add(l_name_1_1);
		
		JLabel l_name_1_2 = new JLabel("Besitzer");
		l_name_1_2.setFont(new Font("Verdana", Font.PLAIN, 13));
		l_name_1_2.setBounds(22, 194, 63, 21);
		frmNeuesHotel.getContentPane().add(l_name_1_2);
		
		JLabel l_name_1_3 = new JLabel("Kontakt");
		l_name_1_3.setFont(new Font("Verdana", Font.PLAIN, 13));
		l_name_1_3.setBounds(22, 234, 63, 21);
		frmNeuesHotel.getContentPane().add(l_name_1_3);
		
		textField_5 = new JTextField();
		textField_5.setColumns(10);
		textField_5.setBounds(352, 160, 195, 27);
		frmNeuesHotel.getContentPane().add(textField_5);
		
		JLabel l_name_1_1_1 = new JLabel("Stadt");
		l_name_1_1_1.setFont(new Font("Verdana", Font.PLAIN, 13));
		l_name_1_1_1.setBounds(279, 163, 63, 21);
		frmNeuesHotel.getContentPane().add(l_name_1_1_1);
		
		JLabel l_name_1_1_1_1 = new JLabel("Telefon");
		l_name_1_1_1_1.setFont(new Font("Verdana", Font.PLAIN, 13));
		l_name_1_1_1_1.setBounds(279, 234, 63, 21);
		frmNeuesHotel.getContentPane().add(l_name_1_1_1_1);
		
		textField_6 = new JTextField();
		textField_6.setColumns(10);
		textField_6.setBounds(353, 231, 195, 27);
		frmNeuesHotel.getContentPane().add(textField_6);
		
		textField_7 = new JTextField();
		textField_7.setColumns(10);
		textField_7.setBounds(112, 268, 121, 27);
		frmNeuesHotel.getContentPane().add(textField_7);
		
		textField_8 = new JTextField();
		textField_8.setColumns(10);
		textField_8.setBounds(353, 268, 195, 27);
		frmNeuesHotel.getContentPane().add(textField_8);
		
		JLabel l_name_1_3_1 = new JLabel("Raumanzahl");
		l_name_1_3_1.setFont(new Font("Verdana", Font.PLAIN, 13));
		l_name_1_3_1.setBounds(22, 274, 80, 21);
		frmNeuesHotel.getContentPane().add(l_name_1_3_1);
		
		JLabel l_name_1_3_1_1 = new JLabel("Bettenanzahl");
		l_name_1_3_1_1.setFont(new Font("Verdana", Font.PLAIN, 13));
		l_name_1_3_1_1.setBounds(260, 274, 87, 21);
		frmNeuesHotel.getContentPane().add(l_name_1_3_1_1);
		textField_3.setColumns(10);
		textField_3.setBounds(112, 194, 436, 27);
		
		frmNeuesHotel.getContentPane().add(textField_3);
		
		JComboBox comboBox = new JComboBox();
		comboBox.addKeyListener(new KeyAdapter() {
			
			public void keyPressed(KeyEvent e) {
			}
		});
		comboBox.addItem("*");
		comboBox.addItem("**");
		comboBox.addItem("***");
		comboBox.addItem("****");
		comboBox.addItem("*****");
		comboBox.setSelectedItem(null);
		comboBox.setBounds(112, 10, 63, 34);
		frmNeuesHotel.getContentPane().add(comboBox);
		
		JLabel l_name_2 = new JLabel("Kategorie");
		l_name_2.setFont(new Font("Verdana", Font.PLAIN, 13));
		l_name_2.setBounds(22, 21, 63, 21);
		frmNeuesHotel.getContentPane().add(l_name_2);
		
		JButton btnNewButton = new JButton("Abbrechen");
		btnNewButton.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
			}
		});
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {	
			}
		});
		btnNewButton.setFont(new Font("Dubai", Font.PLAIN, 15));
		btnNewButton.setBounds(372, 339, 121, 50);
		frmNeuesHotel.getContentPane().add(btnNewButton);
		
		JButton btnAnlegen = new JButton("Anlegen");
		btnAnlegen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
			
		});
		
		btnAnlegen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MainWindow.main(null);
			}
			
		});
		
	
		btnAnlegen.setFont(new Font("Dubai", Font.PLAIN, 15));
		btnAnlegen.setBounds(112, 339, 121, 50);
		frmNeuesHotel.getContentPane().add(btnAnlegen);
		
		
	
		
	}

	protected void setDefaultCloseOperation() {
		// TODO Auto-generated method stub
		
	}

	protected static void setDefaultCloseOperation(int exitOnClose) {
		// TODO Auto-generated method stub
		
	}
}
