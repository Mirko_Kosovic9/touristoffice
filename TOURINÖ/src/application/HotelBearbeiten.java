package application;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class HotelBearbeiten {

	private JFrame frame;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_6;
	private JTextField textField_7;
	private JTextField textField_8;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					HotelBearbeiten window = new HotelBearbeiten();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public HotelBearbeiten() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 590, 408);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		textField = new JTextField();
		textField.setBounds(70, 79, 494, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Name");
		lblNewLabel.setBounds(4, 82, 46, 14);
		frame.getContentPane().add(lblNewLabel);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(70, 110, 494, 20);
		frame.getContentPane().add(textField_1);
		
		JLabel lblAdresse = new JLabel("Adresse");
		lblAdresse.setBounds(4, 113, 46, 14);
		frame.getContentPane().add(lblAdresse);
		
		JLabel lblPlz = new JLabel("PLZ");
		lblPlz.setBounds(4, 144, 46, 14);
		frame.getContentPane().add(lblPlz);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(70, 141, 109, 20);
		frame.getContentPane().add(textField_2);
		
		JLabel lblStadt = new JLabel("Stadt");
		lblStadt.setBounds(239, 144, 46, 14);
		frame.getContentPane().add(lblStadt);
		
		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(284, 141, 280, 20);
		frame.getContentPane().add(textField_3);
		
		textField_4 = new JTextField();
		textField_4.setColumns(10);
		textField_4.setBounds(70, 172, 494, 20);
		frame.getContentPane().add(textField_4);
		
		JLabel lblBesitzer = new JLabel("Besitzer");
		lblBesitzer.setBounds(4, 175, 46, 14);
		frame.getContentPane().add(lblBesitzer);
		
		JLabel lblTelefon = new JLabel("Telefon");
		lblTelefon.setBounds(4, 206, 46, 14);
		frame.getContentPane().add(lblTelefon);
		
		textField_5 = new JTextField();
		textField_5.setColumns(10);
		textField_5.setBounds(70, 203, 109, 20);
		frame.getContentPane().add(textField_5);
		
		textField_6 = new JTextField();
		textField_6.setColumns(10);
		textField_6.setBounds(294, 203, 280, 20);
		frame.getContentPane().add(textField_6);
		
		JLabel lblKontakt = new JLabel("Kontakt");
		lblKontakt.setBounds(228, 206, 46, 14);
		frame.getContentPane().add(lblKontakt);
		
		textField_7 = new JTextField();
		textField_7.setColumns(10);
		textField_7.setBounds(70, 234, 109, 20);
		frame.getContentPane().add(textField_7);
		
		JLabel lblRaumanzahl = new JLabel("Raumanzahl");
		lblRaumanzahl.setBounds(4, 238, 69, 14);
		frame.getContentPane().add(lblRaumanzahl);
		
		JLabel lblBettenanzahl = new JLabel("Bettenanzahl");
		lblBettenanzahl.setBounds(228, 237, 69, 14);
		frame.getContentPane().add(lblBettenanzahl);
		
		textField_8 = new JTextField();
		textField_8.setColumns(10);
		textField_8.setBounds(294, 234, 280, 20);
		frame.getContentPane().add(textField_8);
		
		JButton btnNewButton = new JButton("OK");
		btnNewButton.setBounds(185, 335, 89, 23);
		frame.getContentPane().add(btnNewButton);
		
		JButton btnAbbrechen = new JButton("Abbrechen");
		btnAbbrechen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			}
		});
		btnAbbrechen.setBounds(284, 335, 89, 23);
		frame.getContentPane().add(btnAbbrechen);
		
		JLabel lblAuswahl = new JLabel("Auswahl");
		lblAuswahl.setBounds(4, 32, 46, 14);
		frame.getContentPane().add(lblAuswahl);
		
		JLabel lblKategorie = new JLabel("Kategorie");
		lblKategorie.setBounds(4, 57, 46, 14);
		frame.getContentPane().add(lblKategorie);
	}
}
