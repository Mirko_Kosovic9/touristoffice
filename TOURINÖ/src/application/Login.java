package application;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Login {

	private JFrame frame;
	private final JLabel lb_name = new JLabel("Benutzername");
	private final JLabel lb_psw = new JLabel("Passwort");
	private final JTextField textField = new JTextField();
	private final JTextField textField_1 = new JTextField();
	private final JButton btnEinlogen = new JButton("Einlogen");

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Login window = new Login();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Login() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		String name = "admin";
		String psw = "admin";
		textField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		
		
		textField.setBounds(178, 137, 162, 30);
		textField.setColumns(10);
		frame = new JFrame();
		frame.setBounds(100, 100, 435, 341);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		lb_name.setFont(new Font("Tahoma", Font.BOLD, 14));
		lb_name.setBounds(24, 79, 111, 36);
		
		frame.getContentPane().add(lb_name);
		lb_psw.setFont(new Font("Tahoma", Font.BOLD, 14));
		lb_psw.setBounds(24, 131, 90, 36);
		
		frame.getContentPane().add(lb_psw);
		
		frame.getContentPane().add(textField);
		textField_1.setColumns(10);
		textField_1.setBounds(178, 85, 162, 30);
		
		frame.getContentPane().add(textField_1);
		btnEinlogen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				
			}
		});
		btnEinlogen.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				
	
			}
		});
		btnEinlogen.setFont(new Font("Dubai", Font.BOLD, 17));
		btnEinlogen.setBounds(178, 219, 162, 36);
		
		frame.getContentPane().add(btnEinlogen);
	}
}
