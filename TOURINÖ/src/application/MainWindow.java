package application;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.Color;
import java.awt.Toolkit;
import java.awt.FlowLayout;
import javax.swing.BoxLayout;

public class MainWindow {

	private JFrame frmTouristOfficeN;
	private final JMenuBar menuBar = new JMenuBar();
	private final JButton btnNewButton = new JButton("Hotel");
	private final JButton btnNewButton_1 = new JButton("Belegung");

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow window = new MainWindow();
					window.frmTouristOfficeN.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmTouristOfficeN = new JFrame();
		frmTouristOfficeN.setTitle("Tourist Office NÖ");
		frmTouristOfficeN.setForeground(Color.LIGHT_GRAY);
		frmTouristOfficeN.getContentPane().setForeground(Color.LIGHT_GRAY);
		frmTouristOfficeN.setBackground(Color.LIGHT_GRAY);
		frmTouristOfficeN.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
			}
		});
		frmTouristOfficeN.setBounds(100, 100, 702, 307);
		frmTouristOfficeN.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		menuBar.setToolTipText("Hotel");
		
		frmTouristOfficeN.setJMenuBar(menuBar);
		/*btnNewButton.addActionListener(new ActionListener() {		
			public void actionPerformed(ActionEvent arg0) {
				
				HotelWindow.main(null);

			}
		});*/
		
		menuBar.add(btnNewButton);
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		
		menuBar.add(btnNewButton_1);
		
		JButton btnNewButton_2_1 = new JButton("Hotel Bearbeiten");
		btnNewButton_2_1.setBounds(405, 182, 131, 26);
		btnNewButton_2_1.setBackground(new Color(240, 230, 140));
		btnNewButton_2_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			}
		});
		frmTouristOfficeN.getContentPane().setLayout(null);
		
		JButton btnNewButton_2 = new JButton("Hotel Anlegen");
		btnNewButton_2.setBounds(141, 182, 114, 26);
		btnNewButton_2.setBackground(new Color(30, 144, 255));
		frmTouristOfficeN.getContentPane().add(btnNewButton_2);
		
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				HotelWindow.main(null);
			}
		}
				
				);
		frmTouristOfficeN.getContentPane().add(btnNewButton_2_1);
	}
}
