package touristOffice;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import java.awt.SystemColor;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.awt.Color;
import javax.swing.JButton;
import javax.swing.JComboBox;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class UpdateHotel {

	private JFrame frmHotelndern;
	private JTextField idText;
	private JTextField adresseText;
	private JTextField zimmerText;
	private JTextField bettenText;
	private JTextField stadtText;
	private JTextField nameText;
	private JTextField besitzerText;
	private JTextField plzText;
	private JTextField telefonText;
	private JTextField kontaktText;
	private JComboBox comboBoxKat;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UpdateHotel window = new UpdateHotel();
					window.frmHotelndern.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public UpdateHotel() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmHotelndern = new JFrame();
		frmHotelndern.setTitle("Hotel ändern");
		frmHotelndern.setBounds(100, 100, 932, 341);
		frmHotelndern.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmHotelndern.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Daten welches Hotels möchten Sie ändern?");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblNewLabel.setBounds(10, 34, 270, 48);
		frmHotelndern.getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Tragen Sie bitte die Hotel-ID ein!");
		lblNewLabel_1.setForeground(Color.RED);
		lblNewLabel_1.setBounds(291, 20, 198, 13);
		frmHotelndern.getContentPane().add(lblNewLabel_1);
		
		idText = new JTextField();
		idText.setBounds(290, 43, 157, 26);
		frmHotelndern.getContentPane().add(idText);
		idText.setColumns(10);
		
		JButton btnNewButton = new JButton("Hotel anzeigen");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					anzeigen();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnNewButton.setBounds(482, 43, 122, 27);
		frmHotelndern.getContentPane().add(btnNewButton);
		
		comboBoxKat = new JComboBox();
		comboBoxKat.addItem("*");
		comboBoxKat.addItem("**");
		comboBoxKat.addItem("***");
		comboBoxKat.addItem("****");
		comboBoxKat.addItem("*****");
		
		comboBoxKat.setBounds(132, 154, 60, 26);
		frmHotelndern.getContentPane().add(comboBoxKat);
		
		JLabel lblNewLabel_2 = new JLabel("Kategorie");
		lblNewLabel_2.setBounds(23, 153, 102, 26);
		frmHotelndern.getContentPane().add(lblNewLabel_2);
		
		JLabel lblNewLabel_2_2 = new JLabel("Name");
		lblNewLabel_2_2.setBounds(208, 154, 50, 26);
		frmHotelndern.getContentPane().add(lblNewLabel_2_2);
		
		JLabel lblNewLabel_2_3 = new JLabel("Besitzer");
		lblNewLabel_2_3.setBounds(474, 154, 60, 26);
		frmHotelndern.getContentPane().add(lblNewLabel_2_3);
		
		JLabel lblNewLabel_2_4 = new JLabel("Kontakt");
		lblNewLabel_2_4.setBounds(702, 154, 68, 26);
		frmHotelndern.getContentPane().add(lblNewLabel_2_4);
		
		JLabel lblNewLabel_2_5 = new JLabel("Adresse");
		lblNewLabel_2_5.setBounds(23, 193, 102, 26);
		frmHotelndern.getContentPane().add(lblNewLabel_2_5);
		
		JLabel lblNewLabel_2_6 = new JLabel("Stadt");
		lblNewLabel_2_6.setBounds(278, 193, 60, 26);
		frmHotelndern.getContentPane().add(lblNewLabel_2_6);
		
		JLabel lblNewLabel_2_7 = new JLabel("PLZ");
		lblNewLabel_2_7.setBounds(503, 193, 31, 26);
		frmHotelndern.getContentPane().add(lblNewLabel_2_7);
		
		JLabel lblNewLabel_2_8 = new JLabel("Anzahl der Zimmer");
		lblNewLabel_2_8.setBounds(23, 229, 129, 26);
		frmHotelndern.getContentPane().add(lblNewLabel_2_8);
		
		JLabel lblNewLabel_2_8_1 = new JLabel("Telefon");
		lblNewLabel_2_8_1.setBounds(702, 193, 60, 26);
		frmHotelndern.getContentPane().add(lblNewLabel_2_8_1);
		
		JLabel lblNewLabel_2_8_2 = new JLabel("Anzahl der Betten");
		lblNewLabel_2_8_2.setBounds(278, 229, 112, 26);
		frmHotelndern.getContentPane().add(lblNewLabel_2_8_2);
		
		adresseText = new JTextField();
		adresseText.setColumns(10);
		adresseText.setBounds(72, 190, 196, 29);
		frmHotelndern.getContentPane().add(adresseText);
		
		zimmerText = new JTextField();
		zimmerText.setColumns(10);
		zimmerText.setBounds(162, 224, 96, 37);
		frmHotelndern.getContentPane().add(zimmerText);
		
		bettenText = new JTextField();
		bettenText.setColumns(10);
		bettenText.setBounds(393, 226, 96, 34);
		frmHotelndern.getContentPane().add(bettenText);
		
		stadtText = new JTextField();
		stadtText.setColumns(10);
		stadtText.setBounds(316, 190, 177, 29);
		frmHotelndern.getContentPane().add(stadtText);
		
		nameText = new JTextField();
		nameText.setColumns(10);
		nameText.setBounds(268, 155, 198, 26);
		frmHotelndern.getContentPane().add(nameText);
		
		besitzerText = new JTextField();
		besitzerText.setColumns(10);
		besitzerText.setBounds(544, 153, 148, 29);
		frmHotelndern.getContentPane().add(besitzerText);
		
		plzText = new JTextField();
		plzText.setColumns(10);
		plzText.setBounds(544, 193, 148, 26);
		frmHotelndern.getContentPane().add(plzText);
		
		telefonText = new JTextField();
		telefonText.setColumns(10);
		telefonText.setBounds(769, 193, 139, 26);
		frmHotelndern.getContentPane().add(telefonText);
		
		kontaktText = new JTextField();
		kontaktText.setColumns(10);
		kontaktText.setBounds(769, 154, 139, 26);
		frmHotelndern.getContentPane().add(kontaktText);
		
		JButton btnnderungenSpeichern = new JButton("Änderungen speichern");
		btnnderungenSpeichern.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
					try {
						updateHotel();
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
			}
		});
		btnnderungenSpeichern.setBounds(512, 240, 183, 36);
		frmHotelndern.getContentPane().add(btnnderungenSpeichern);
	}
	
	public void anzeigen() throws SQLException {
		Connection con = null;
		PreparedStatement statement = null;
		
		DefaultTableModel model = new DefaultTableModel();
		model.addColumn("ID");
		model.addColumn("Kategorie");
		model.addColumn("Name");
		model.addColumn("Besitzer");
		model.addColumn("Kontakt");
		model.addColumn("Adresse");
		model.addColumn("Stadt");
		model.addColumn("PLZ");
		model.addColumn("Telefon");
		model.addColumn("Raumanzahl");
		model.addColumn("Bettenanzahl");
		
		con = DatabaseConnection.getConnection();
		
		String query = "SELECT * FROM Hotels WHERE HotelID = ?";
		statement = con.prepareStatement(query);
		
		statement.setString(1, idText.getText());
		
		ResultSet rs = statement.executeQuery();
		
		if(rs.next()) {
			
			comboBoxKat.setSelectedItem(rs.getString("Kategorie"));
			nameText.setText(rs.getString("Name"));
			besitzerText.setText(rs.getString("Besitzer"));
			kontaktText.setText(rs.getString("Kontakt"));
			adresseText.setText(rs.getString("Adresse"));
			stadtText.setText(rs.getString("Stadt"));
			plzText.setText(rs.getString("PLZ"));
			telefonText.setText(rs.getString("Telefon"));
			zimmerText.setText(rs.getString("ZimmerNr"));
			bettenText.setText(rs.getString("BettenNr"));
	}
	
		
		// ********* TEST for return **********
		//	System.out.println(rs.getString("Kategorie") + " " + rs.getString("Name"));
	}
	public void updateHotel() throws SQLException {
		
		Connection con = DatabaseConnection.getConnection();
		PreparedStatement statement = null;
		
		String query = " UPDATE Hotels SET Kategorie = ?, Name = ?, Besitzer = ?, Kontakt = ?, Adresse = ?, Stadt = ?,"
				+ "PLZ = ?, Telefon = ?, ZimmerNr = ?, BettenNr = ? WHERE HotelID = ? ";
		
		statement = con.prepareStatement(query);
		
		
		statement.setString(1, (String) comboBoxKat.getSelectedItem());
		statement.setString(2, nameText.getText());
		statement.setString(3, besitzerText.getText());
		statement.setString(4, kontaktText.getText());
		statement.setString(5, adresseText.getText());
		statement.setString(6, stadtText.getText());
		statement.setString(7, plzText.getText());
		statement.setString(8, telefonText.getText());
		statement.setString(9, zimmerText.getText());
		statement.setString(10, bettenText.getText());
		statement.setString(11, idText.getText());
		
		int rs = statement.executeUpdate();
		
		if(rs==1) {
			JOptionPane.showMessageDialog(null, "Erfolgreich geändert");
			comboBoxKat.setSelectedItem("*");
			nameText.setText("");
			besitzerText.setText("");
			kontaktText.setText("");
			adresseText.setText("");
			stadtText.setText("");
			plzText.setText("");
			telefonText.setText("");
			zimmerText.setText("");
			bettenText.setText("");
			
			idText.setText("");
		}
		else {
			JOptionPane.showMessageDialog(null, "Daten überprüfen!");
		}
		
	}

}
