package touristOffice;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.awt.event.ActionEvent;

public class UpdateWindow {

	private JFrame frmDatenndern;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UpdateWindow window = new UpdateWindow();
					window.frmDatenndern.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public UpdateWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmDatenndern = new JFrame();
		frmDatenndern.setTitle("Daten ändern");
		frmDatenndern.setBounds(100, 100, 450, 300);
		frmDatenndern.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		JButton btnExtrasDaten = new JButton("Extrasdaten ändern");
		btnExtrasDaten.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				UpdateExtras.main(null);
				frmDatenndern.dispose();
			}
		});
		btnExtrasDaten.setFont(new Font("Tahoma", Font.BOLD, 12));
		frmDatenndern.getContentPane().add(btnExtrasDaten, BorderLayout.WEST);
		
		JButton btnHotelDaten = new JButton("Hoteldaten ändern");
		btnHotelDaten.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				UpdateHotel.main(null);
				frmDatenndern.dispose();
			}
		});
		btnHotelDaten.setFont(new Font("Tahoma", Font.BOLD, 12));
		frmDatenndern.getContentPane().add(btnHotelDaten, BorderLayout.EAST);
		
		JButton btnNewButton = new JButton("Extras hinzufügen");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addExtrasToHotel.main(null);
				frmDatenndern.dispose();
			}
		});
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 12));
		frmDatenndern.getContentPane().add(btnNewButton, BorderLayout.CENTER);
	}
	
}
