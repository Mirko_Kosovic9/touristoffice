package touristOffice;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.event.ActionListener;
import java.io.File;
import java.awt.event.ActionEvent;

public class Handbuch {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Handbuch window = new Handbuch();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Handbuch() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JButton PDFAnzeigen = new JButton("PDF Handbuch Anzeigen\r\n");
		PDFAnzeigen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			
				
				//open existing pdf file
				try {
					File file = new File("C:\\Users\\Niksa Marinkovic\\Desktop\\Niksa\\Faks\\4. Semester\\Project Management Cycle\\User_manual.pdf");
					
					//check if file exists then open it
					if(file.exists()) {
						if(Desktop.isDesktopSupported()) {
							Desktop.getDesktop().open(file);
						}else {
							
							JOptionPane.showMessageDialog(PDFAnzeigen, this, "Not supported!", 0);

							}
					}
							else {
					
						JOptionPane.showMessageDialog(PDFAnzeigen, this, "File does not exist!", 0);
							}
						
				}catch(Exception pdf) {
					pdf.printStackTrace();
				}
					
				}
			
		});
		PDFAnzeigen.setBounds(251, 227, 173, 23);
		frame.getContentPane().add(PDFAnzeigen);
	}
}
