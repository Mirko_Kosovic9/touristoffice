package touristOffice;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;

import java.awt.Font;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class HotelAnlegen extends JFrame {

	private JFrame frmHotelAnlegen;
	private JTextField raumanzahl;
	private JTextField adresse;
	private JTextField kontakt;
	private JTextField besitzer;
	private JTextField name;
	private JTextField bettenanzahl;
	private JTextField plz;
	private JTextField stadt;
	private JTextField telefon;
	private JButton btnAbbrechen;
	private JButton btnAnlegen;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					HotelAnlegen window = new HotelAnlegen();
					window.frmHotelAnlegen.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public HotelAnlegen() {
		initialize();
	}

	private void initialize() {
		frmHotelAnlegen = new JFrame();
		frmHotelAnlegen.setTitle("HOTEL ANLEGEN");
		frmHotelAnlegen.setBounds(100, 100, 823, 425);
		frmHotelAnlegen.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmHotelAnlegen.getContentPane().setLayout(null);

		JLabel lblNewLabel = new JLabel("Name");
		lblNewLabel.setFont(new Font("Dubai", Font.BOLD, 15));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(21, 101, 121, 34);
		frmHotelAnlegen.getContentPane().add(lblNewLabel);

		JLabel lblBesitzer = new JLabel("Besitzer");
		lblBesitzer.setHorizontalAlignment(SwingConstants.CENTER);
		lblBesitzer.setFont(new Font("Dubai", Font.BOLD, 15));
		lblBesitzer.setBounds(21, 150, 121, 34);
		frmHotelAnlegen.getContentPane().add(lblBesitzer);

		JLabel lblKontakt = new JLabel("Kontakt");
		lblKontakt.setHorizontalAlignment(SwingConstants.CENTER);
		lblKontakt.setFont(new Font("Dubai", Font.BOLD, 15));
		lblKontakt.setBounds(21, 194, 121, 34);
		frmHotelAnlegen.getContentPane().add(lblKontakt);

		JLabel lblAdresse = new JLabel("Adresse");
		lblAdresse.setHorizontalAlignment(SwingConstants.CENTER);
		lblAdresse.setFont(new Font("Dubai", Font.BOLD, 15));
		lblAdresse.setBounds(21, 238, 121, 34);
		frmHotelAnlegen.getContentPane().add(lblAdresse);

		JLabel lblTelefon = new JLabel("Telefon");
		lblTelefon.setHorizontalAlignment(SwingConstants.CENTER);
		lblTelefon.setFont(new Font("Dubai", Font.BOLD, 15));
		lblTelefon.setBounds(331, 150, 121, 34);
		frmHotelAnlegen.getContentPane().add(lblTelefon);

		JLabel lblNewLabel_5_1 = new JLabel("Stadt");
		lblNewLabel_5_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_5_1.setFont(new Font("Dubai", Font.BOLD, 15));
		lblNewLabel_5_1.setBounds(331, 194, 121, 34);
		frmHotelAnlegen.getContentPane().add(lblNewLabel_5_1);

		JLabel lblNewLabel_5_2 = new JLabel("PLZ");
		lblNewLabel_5_2.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_5_2.setFont(new Font("Dubai", Font.BOLD, 15));
		lblNewLabel_5_2.setBounds(331, 238, 121, 34);
		frmHotelAnlegen.getContentPane().add(lblNewLabel_5_2);

		JLabel lblNewLabel_5_2_1 = new JLabel("Anzahl der Zimmer");
		lblNewLabel_5_2_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_5_2_1.setFont(new Font("Dubai", Font.BOLD, 15));
		lblNewLabel_5_2_1.setBounds(21, 282, 121, 34);
		frmHotelAnlegen.getContentPane().add(lblNewLabel_5_2_1);

		JLabel lblNewLabel_5_2_2 = new JLabel("Anzahl der Betten");
		lblNewLabel_5_2_2.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_5_2_2.setFont(new Font("Dubai", Font.BOLD, 15));
		lblNewLabel_5_2_2.setBounds(319, 282, 147, 34);
		frmHotelAnlegen.getContentPane().add(lblNewLabel_5_2_2);

		JComboBox<String> kategorie = new JComboBox<String>();
		kategorie.addItem("*");
		kategorie.addItem("**");
		kategorie.addItem("***");
		kategorie.addItem("****");
		kategorie.addItem("*****");
		kategorie.setBounds(473, 47, 147, 41);
		frmHotelAnlegen.getContentPane().add(kategorie);

		JLabel lblKategorie = new JLabel("Kategorie");
		lblKategorie.setHorizontalAlignment(SwingConstants.CENTER);
		lblKategorie.setFont(new Font("Dubai", Font.BOLD, 15));
		lblKategorie.setBounds(331, 50, 121, 34);
		frmHotelAnlegen.getContentPane().add(lblKategorie);

		raumanzahl = new JTextField();
		raumanzahl.setBounds(152, 286, 147, 27);
		frmHotelAnlegen.getContentPane().add(raumanzahl);
		raumanzahl.setColumns(10);

		adresse = new JTextField();
		adresse.setColumns(10);
		adresse.setBounds(152, 242, 147, 27);
		frmHotelAnlegen.getContentPane().add(adresse);

		kontakt = new JTextField();
		kontakt.setColumns(10);
		kontakt.setBounds(152, 198, 147, 27);
		frmHotelAnlegen.getContentPane().add(kontakt);

		besitzer = new JTextField();
		besitzer.setColumns(10);
		besitzer.setBounds(152, 154, 147, 27);
		frmHotelAnlegen.getContentPane().add(besitzer);

		name = new JTextField();
		name.setColumns(10);
		name.setBounds(152, 105, 147, 27);
		frmHotelAnlegen.getContentPane().add(name);

		bettenanzahl = new JTextField();
		bettenanzahl.setColumns(10);
		bettenanzahl.setBounds(473, 286, 147, 27);
		frmHotelAnlegen.getContentPane().add(bettenanzahl);

		plz = new JTextField();
		plz.setColumns(10);
		plz.setBounds(473, 242, 147, 27);
		frmHotelAnlegen.getContentPane().add(plz);

		stadt = new JTextField();
		stadt.setColumns(10);
		stadt.setBounds(473, 194, 147, 27);
		frmHotelAnlegen.getContentPane().add(stadt);

		telefon = new JTextField();
		telefon.setColumns(10);
		telefon.setBounds(473, 150, 147, 27);
		frmHotelAnlegen.getContentPane().add(telefon);

		btnAbbrechen = new JButton("Abbrechen");
		btnAbbrechen.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
			}
		});
		btnAbbrechen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					HotelAnlegen.main(null);
				} catch (Exception e) {
					e.printStackTrace();
				}
				// frmHotelAnlegen.dispose();

			}
		});

		// JButton ExitYes = new JButton("Yes");
		btnAbbrechen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int confirmed = JOptionPane.showConfirmDialog(null,
						"Sie sind sicher, dass Sie beenden wollen? Alle nicht gespeicherten Änderungen gehen verloren!",
						"Exit Program", JOptionPane.YES_NO_OPTION);

				if (confirmed == JOptionPane.YES_OPTION) {
					// frmHotelAnlegen.dispose();
					System.exit(confirmed);

				} else {
					dispose();
				}
			}
		});

		btnAbbrechen.setBounds(85, 337, 214, 41);
		frmHotelAnlegen.getContentPane().add(btnAbbrechen);

		btnAnlegen = new JButton("Anlegen");
		btnAnlegen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				Connection con = null;
				PreparedStatement queryStatement = null;

				if (name.getText().isEmpty() || besitzer.getText().isEmpty() || kontakt.getText().isEmpty()
						|| adresse.getText().isEmpty() || stadt.getText().isEmpty()
						|| plz.getText().isEmpty() || telefon.getText().isEmpty() || raumanzahl.getText().isEmpty()
						|| bettenanzahl.getText().isEmpty()) {

					JOptionPane.showMessageDialog(null, "Alle Felder muessen einen Wert haben!");
				}

				else {

					System.out
							.println("Executing query: " + (String) kategorie.getSelectedItem() + " ," + name.getText()
									+ "," + besitzer.getText() + "," + kontakt.getText() + "," + adresse.getText()
									+ "," + stadt.getText() + "," + plz.getText() + ","
									+ telefon.getText() + "," + raumanzahl.getText() + " ," + bettenanzahl.getText());

					try {
						String statement = "INSERT INTO Hotels values ("
								+ "	CASE \r\n"
								+ "		WHEN  (SELECT MIN(h.HotelID) FROM Hotels h) = 2   THEN 1\r\n"
								+ "		ELSE\r\n"
								+ "			(SELECT MIN(h.HotelID + 1)\r\n"
								+ "			FROM Hotels h LEFT JOIN Hotels h2 ON H.HotelID + 1 = h2.HotelID\r\n"
								+ "			WHERE h2.HotelID IS NULL)\r\n"
								+ "	END,"
								+ "?,?,?,?,?,?,?,?,?,?)";
						
						
						
						
						con = DatabaseConnection.getConnection();
						queryStatement = con.prepareStatement(statement);
					} catch (SQLException e1) {
						JOptionPane.showMessageDialog(null, "Problem mit der Verbindung!");
					}
					try {
						queryStatement.setString(1, (String) kategorie.getSelectedItem());
						queryStatement.setString(2, name.getText());
						queryStatement.setString(3, besitzer.getText());
						queryStatement.setString(4, kontakt.getText());
						queryStatement.setString(5, adresse.getText());
						queryStatement.setString(6, stadt.getText());
						queryStatement.setString(7, plz.getText());
						queryStatement.setString(8, telefon.getText());
						queryStatement.setString(9, raumanzahl.getText());
						queryStatement.setString(10, bettenanzahl.getText());

					} catch (Exception e1) {

						JOptionPane.showMessageDialog(null, "Falsche Eingabe beim Anlegen!");
					}

					int countOfRows = 0;

					try {
						countOfRows = queryStatement.executeUpdate();
					} catch (SQLException e2) {
						e2.printStackTrace();
					}
					try {
						boolean rs = queryStatement.getMoreResults();
					} catch (SQLException e1) {
						e1.printStackTrace();
					}

					System.out.println("Hotel saved " + countOfRows);

					int ask = JOptionPane.showConfirmDialog(btnAnlegen, "Wollen Sie noch ein Hotel Anlegen?",
							"Bestaetigen", 0);

					if (ask == JOptionPane.YES_OPTION) {
						kategorie.setSelectedIndex(0);
						name.setText("");
						besitzer.setText("");
						kontakt.setText("");
						adresse.setText("");
						stadt.setText("");
						plz.setText("");
						telefon.setText("");
						raumanzahl.setText("");
						bettenanzahl.setText("");
					} else if (ask == JOptionPane.NO_OPTION) {
						frmHotelAnlegen.dispose();
					} else {

					}
				}

			}
		});
		btnAnlegen.setBounds(406, 337, 214, 41);
		frmHotelAnlegen.getContentPane().add(btnAnlegen);
	}
}
