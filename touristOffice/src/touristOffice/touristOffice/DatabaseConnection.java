package touristOffice;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseConnection {

	static String url = "jdbc:sqlserver://";
	static String serverName = "localhost";
	static String portNumber = "1433";
	static String dbName = "tourist";
	static String userName = "bob";
	static String pwd = "bob";

	public static void main(String[] args) {

		try {
			Connection con = getConnection(userName, pwd);

			System.out.println(con);

			// System.out.println(connectionUrl());
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}

	}

	public static Connection getConnection(String _user, String _pwd) throws SQLException {

		Connection con = null;

		con = DriverManager.getConnection(connectionUrl(), _user, _pwd);

		return con;
	}

	public static Connection getConnection() throws SQLException {

		Connection con = null;

		con = DriverManager.getConnection(connectionUrl(), userName, pwd);

		return con;
	}

	private static String connectionUrl() {

		return url + serverName + ":" + portNumber + ";databaseName=" + dbName;
	}

}
