package touristOffice;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.JOptionPane;

public class DBTest {

	public static void main(String[] args) throws SQLException {

		// ArrayList<Mitarbeiter> allStaff = getAllStaff();
		// allStaff.forEach(System.out::println);
		//
		// Mitarbeiter m = getStuffById(1);
		//
		// if (m != null) {
		// System.out.println(m.toString());
		// }

		// Mitarbeiter heinz = new Mitarbeiter(4, "Karl", "Fischer")

		// insertManyAtOnes(newStaff);

		// updateMitarbeiter(1, "Realname");

	}

	private static void deletePersonByID(int id) {

		String DELETE_STATEMENT = "delete from staff where id = ?";

		try (Connection connection = DatabaseConnection.getConnection();
				PreparedStatement statement = connection.prepareStatement(DELETE_STATEMENT)) {

			statement.setInt(1, id);

			int rows = statement.executeUpdate();
			System.out.println(rows);

			if (rows == 0) {
				JOptionPane.showMessageDialog(null, "No person was deleted");
			}

			System.out.println();

			System.out.println("done");

		} catch (SQLException e) {
			System.out.println(e.getErrorCode());

		}

	}

	private static void updateMitarbeiter(int pk, String name) {

		String UPDATE_STATEMENT = "update staff set name = ? where id = ?";

		try (Connection connection = DatabaseConnection.getConnection();
				PreparedStatement statement = connection.prepareStatement(UPDATE_STATEMENT)) {

			statement.setString(1, name);
			statement.setInt(2, pk);

			int rows = statement.executeUpdate();
			System.out.println(rows);

			if (rows == 0) {
				JOptionPane.showMessageDialog(null, "No person was found, no update at all");

			}

			System.out.println();

			System.out.println("done");

		} catch (SQLException e) {
			System.out.println(e.getErrorCode());

		}

	}

	private static void insertNewStaff(int id, String name) throws SQLException {

		Connection con = null;
		PreparedStatement queryStatement = null;

		String statement = "INSERT INTO staff values (?,?)";

		System.out.println("Executing query: " + statement);

		con = DatabaseConnection.getConnection();
		queryStatement = con.prepareStatement(statement);

		queryStatement.setInt(1, id);
		queryStatement.setString(2, name);

		int countOfRows = queryStatement.executeUpdate();

		ResultSet rs = queryStatement.getGeneratedKeys();

		System.out.println("Staff saved " + countOfRows);

	}

}
