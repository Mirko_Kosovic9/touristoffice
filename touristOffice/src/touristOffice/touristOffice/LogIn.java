package touristOffice;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;


import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class LogIn {

	private JFrame frmAnmelden;
	private JTextField tf_benutzer;
	private JTextField tf_passwort;

	/**
	 * Launch the application.
	 * 
	 * @wbp.parser.entryPoint
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LogIn window = new LogIn();
					window.frmAnmelden.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * @wbp.parser.entryPoint
	 */
	public LogIn() {
		initialize();

	}

	/**
	 * @wbp.parser.entryPoint
	 */
	private void initialize() {
		frmAnmelden = new JFrame();
		frmAnmelden.setTitle("Anmelden");
		frmAnmelden.setBounds(100, 100, 537, 366);
		frmAnmelden.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmAnmelden.getContentPane().setLayout(null);

		JButton btnUser = new JButton("Als AppUser anmelden");
		btnUser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				anmeldenUser();
			}
		});
		btnUser.setBounds(229, 207, 169, 33);
		frmAnmelden.getContentPane().add(btnUser);

		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setVerticalAlignment(SwingConstants.BOTTOM);
		lblNewLabel.setToolTipText("");
		lblNewLabel.setIcon(new ImageIcon("C:\\Users\\kosov\\git\\touristoffice\\touristOffice\\src\\touristOffice\\LOGO1.png"));
		lblNewLabel.setBounds(0, 0, 522, 121);
		frmAnmelden.getContentPane().add(lblNewLabel);

		JButton btnBesitzer = new JButton("Als Besitzer anmelden");
		btnBesitzer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				anmeldenBesitzer();
			}
		});
		btnBesitzer.setBounds(229, 243, 169, 33);
		frmAnmelden.getContentPane().add(btnBesitzer);

		tf_benutzer = new JTextField();
		tf_benutzer.setBounds(229, 131, 169, 28);
		frmAnmelden.getContentPane().add(tf_benutzer);
		tf_benutzer.setColumns(10);

		tf_passwort = new JPasswordField();
		tf_passwort.setColumns(10);
		tf_passwort.setBounds(229, 169, 169, 28);
		frmAnmelden.getContentPane().add(tf_passwort);

		JLabel lblBenutzer = new JLabel("Benutzername:");
		lblBenutzer.setBounds(118, 131, 101, 27);
		frmAnmelden.getContentPane().add(lblBenutzer);

		JLabel lblPasswort = new JLabel("Passwort:");
		lblPasswort.setBounds(128, 168, 101, 27);
		frmAnmelden.getContentPane().add(lblPasswort);

		JButton btnReg = new JButton("Registrieren Sie sich");
		btnReg.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				RegisterForm.main(null);
			}
		});
		btnReg.setBounds(229, 286, 169, 33);
		frmAnmelden.getContentPane().add(btnReg);
		
		JLabel lblNewLabel_1 = new JLabel("New label");
		lblNewLabel_1.setBounds(10, 11, 501, 110);
		frmAnmelden.getContentPane().add(lblNewLabel_1);
	}

	public void anmeldenUser() {
		Connection con = null;
		try {
			con = DatabaseConnection.getConnection();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		String query = "SELECT Benutzername,Passwort FROM SeniorUserLogin WHERE Benutzername=? AND Passwort=?";

		try {
			PreparedStatement st = null;

			con = DatabaseConnection.getConnection();
			st = con.prepareStatement(query);

			st.setString(1, tf_benutzer.getText());
			st.setString(2, tf_passwort.getText());

			ResultSet rs = st.executeQuery();

			if (rs.next()) {
				JOptionPane.showMessageDialog(null, "Erfolgreiche Anmeldung!");
				UserHome hw = new UserHome();
				hw.main(null);
				frmAnmelden.dispose();
			} else {
				JOptionPane.showMessageDialog(null, "Inkorrekte Daten eingegeben!");
			}
		} catch (Exception e) {
			e.getLocalizedMessage();
		}
	}

	public void anmeldenBesitzer() {
		Connection con = null;
		try {
			con = DatabaseConnection.getConnection();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		String query = "SELECT Benutzername,Passwort FROM BesitzerLogin WHERE Benutzername=? AND Passwort=?";

		try {
			PreparedStatement st = null;

			con = DatabaseConnection.getConnection();
			st = con.prepareStatement(query);

			st.setString(1, tf_benutzer.getText());
			st.setString(2, tf_passwort.getText());

			ResultSet rs = st.executeQuery();

			if (rs.next()) {
				JOptionPane.showMessageDialog(null, "Erfolgreiche Anmeldung!");
				BesitzerHome hw = new BesitzerHome();
				hw.main(null);
				frmAnmelden.dispose();

			} else {
				JOptionPane.showMessageDialog(null, "Inkorrekte Daten eingegeben!");
			}
		} catch (Exception e) {
			e.getLocalizedMessage();
		}
	}
}
