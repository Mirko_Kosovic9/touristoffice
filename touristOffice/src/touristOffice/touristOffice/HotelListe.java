package touristOffice;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.ComboBoxEditor;
import javax.swing.JButton;
import javax.swing.table.DefaultTableModel;

import javax.swing.JScrollPane;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.border.SoftBevelBorder;
import javax.swing.border.BevelBorder;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JComboBox;
import javax.swing.JPopupMenu;
import java.awt.Component;
import java.awt.Button;
import javax.swing.SwingConstants;

public class HotelListe {

	private JFrame frmListeAllerHotels;
	private JTable table;
	private JScrollPane scrollPane;
	private JTextField IdText;
	private JTextField nameField;
	private JTextField textDeleteID;
	private JComboBox comboHund = new JComboBox();
	private JComboBox comboFitness = new JComboBox();
	private JComboBox comboSpa = new JComboBox();
	private JComboBox comboFamilie = new JComboBox();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					HotelListe window = new HotelListe();
					window.hotelListe();
					window.frmListeAllerHotels.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public HotelListe() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmListeAllerHotels = new JFrame();
		frmListeAllerHotels.setTitle("Liste aller Hotels");
		frmListeAllerHotels.setBounds(100, 100, 1193, 562);
		frmListeAllerHotels.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmListeAllerHotels.getContentPane().setLayout(null);

		scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 114, 1169, 309);
		frmListeAllerHotels.getContentPane().add(scrollPane);

		table = new JTable();
		table.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
			}
		});
		table.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
		table.setCellSelectionEnabled(true);
		table.setColumnSelectionAllowed(true);
		scrollPane.setViewportView(table);

		JButton btAnzeigen = new JButton("Hotel anzeigen");
		btAnzeigen.setFont(new Font("Dubai", Font.PLAIN, 15));
		btAnzeigen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					hotelAnzeigen();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		btAnzeigen.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
			}
		});
		btAnzeigen.setBounds(322, 38, 130, 35);
		frmListeAllerHotels.getContentPane().add(btAnzeigen);

		JButton btDelete = new JButton("Hotel Löschen");
		btDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				if (!textDeleteID.getText().isEmpty()) {
					int ask = JOptionPane
							.showConfirmDialog(
									btDelete, "Sind Sie sicher dass Sie das Hotel mit der ID Nummer: "
											+ textDeleteID.getText() + " löschen möchten?",
									"Hotel löschen", JOptionPane.YES_NO_OPTION);

					if (ask == JOptionPane.YES_OPTION) {
						try {
							hotelLöschen();
							textDeleteID.setText("");
						} catch (Exception e) {
							e.printStackTrace();
						}
					} else if (ask == JOptionPane.NO_OPTION) {

					} else {

					}
				} else {
					JOptionPane.showMessageDialog(null, "Sie haben keine ID-Nummer eingetragen!");
				}
			}
		});
		btDelete.setFont(new Font("Dubai", Font.PLAIN, 15));
		btDelete.setBounds(1001, 47, 156, 44);
		frmListeAllerHotels.getContentPane().add(btDelete);

		JLabel HotelID = new JLabel("Hotel ID:");
		HotelID.setFont(new Font("Dialog", Font.BOLD, 14));
		HotelID.setBounds(23, 20, 91, 25);
		frmListeAllerHotels.getContentPane().add(HotelID);

		IdText = new JTextField();
		IdText.setBounds(132, 16, 156, 35);
		frmListeAllerHotels.getContentPane().add(IdText);
		IdText.setColumns(10);

		nameField = new JTextField();
		nameField.setColumns(10);
		nameField.setBounds(132, 52, 156, 35);
		frmListeAllerHotels.getContentPane().add(nameField);

		JLabel lblHotelname = new JLabel("Hotelname:");
		lblHotelname.setFont(new Font("Dialog", Font.BOLD, 14));
		lblHotelname.setBounds(23, 57, 91, 25);
		frmListeAllerHotels.getContentPane().add(lblHotelname);

		JLabel HotelIDdelete = new JLabel("Hotel ID:");
		HotelIDdelete.setFont(new Font("Dialog", Font.BOLD, 14));
		HotelIDdelete.setBounds(921, 10, 77, 29);
		frmListeAllerHotels.getContentPane().add(HotelIDdelete);

		textDeleteID = new JTextField();
		textDeleteID.setColumns(10);
		textDeleteID.setBounds(1001, 10, 156, 27);
		frmListeAllerHotels.getContentPane().add(textDeleteID);

		JLabel lblNewLabel = new JLabel("Hundefreundlich:");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblNewLabel.setBounds(496, 21, 121, 25);
		frmListeAllerHotels.getContentPane().add(lblNewLabel);

		JLabel lblSpa = new JLabel("Spa:");
		lblSpa.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblSpa.setBounds(738, 21, 49, 25);
		frmListeAllerHotels.getContentPane().add(lblSpa);

		JLabel lblFamilienfreundlich = new JLabel("Familienfreundlich:");
		lblFamilienfreundlich.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblFamilienfreundlich.setBounds(496, 48, 121, 25);
		frmListeAllerHotels.getContentPane().add(lblFamilienfreundlich);

		JLabel lblFitness = new JLabel("Fitness:");
		lblFitness.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblFitness.setBounds(724, 51, 63, 22);
		frmListeAllerHotels.getContentPane().add(lblFitness);

		comboFamilie.setBounds(627, 48, 77, 25);
		frmListeAllerHotels.getContentPane().add(comboFamilie);
		comboFamilie.addItem("");
		comboFamilie.addItem("JA");
		comboFamilie.addItem("NEIN");

		comboSpa.setBounds(785, 20, 77, 24);
		frmListeAllerHotels.getContentPane().add(comboSpa);
		comboSpa.addItem("");
		comboSpa.addItem("JA");
		comboSpa.addItem("NEIN");

		comboFitness.setBounds(785, 47, 77, 26);
		frmListeAllerHotels.getContentPane().add(comboFitness);
		comboFitness.addItem("");
		comboFitness.addItem("JA");
		comboFitness.addItem("NEIN");

		comboHund.setBounds(627, 21, 77, 25);
		frmListeAllerHotels.getContentPane().add(comboHund);
		comboHund.addItem("");
		comboHund.addItem("JA");
		comboHund.addItem("NEIN");
		
		JButton btnNewButton = new JButton("Nach Extras suchen");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					extrasAnzeigen();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		btnNewButton.setBounds(546, 79, 272, 25);
		frmListeAllerHotels.getContentPane().add(btnNewButton);
		
		JButton updateData = new JButton("Daten verwalten");
		updateData.setFont(new Font("Dialog", Font.BOLD, 15));
		updateData.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
					UpdateWindow uw = new UpdateWindow();
					uw.main(null);
		
			}
		});
		updateData.setBounds(479, 457, 211, 56);
		frmListeAllerHotels.getContentPane().add(updateData);

	}

	public void hotelListe() throws SQLException {

		Connection con = DatabaseConnection.getConnection();

		DefaultTableModel model = new DefaultTableModel();
		model.addColumn("ID");
		model.addColumn("Kategorie");
		model.addColumn("Name");
		model.addColumn("Besitzer");
		model.addColumn("Kontakt");
		model.addColumn("Adresse");
		model.addColumn("Stadt");
		model.addColumn("PLZ");
		model.addColumn("Telefon");
		model.addColumn("Raumanzahl");
		model.addColumn("Bettenanzahl");

		String query = "SELECT * FROM Hotels";
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery(query);

		while (rs.next()) {
			model.addRow(new Object[] { rs.getInt("HotelID"), rs.getString("Kategorie"), rs.getString("Name"),
					rs.getString("Besitzer"), rs.getString("Kontakt"), rs.getString("Adresse"), rs.getString("Stadt"),
					rs.getInt("PLZ"), rs.getInt("Telefon"), rs.getInt("ZimmerNr"), rs.getInt("BettenNr") });
		}

		table.setModel(model);
		table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		table.getColumnModel().getColumn(0);
		table.getColumnModel().getColumn(1);
		table.getColumnModel().getColumn(2);
		table.getColumnModel().getColumn(3);
		table.getColumnModel().getColumn(4);
		table.getColumnModel().getColumn(5);
		table.getColumnModel().getColumn(6);
		table.getColumnModel().getColumn(7);
		table.getColumnModel().getColumn(8);
		table.getColumnModel().getColumn(9);
		table.getColumnModel().getColumn(10);

	}

	public void hotelAnzeigen() throws SQLException {

		Connection con = null;
		PreparedStatement queryStatement = null;

		DefaultTableModel model = new DefaultTableModel();
		model.addColumn("ID");
		model.addColumn("Kategorie");
		model.addColumn("Name");
		model.addColumn("Besitzer");
		model.addColumn("Kontakt");
		model.addColumn("Adresse");
		model.addColumn("Stadt");
		model.addColumn("PLZ");
		model.addColumn("Telefon");
		model.addColumn("Raumanzahl");
		model.addColumn("Bettenanzahl");

		try {
			if (!IdText.getText().isEmpty()) {

				String query = "SELECT * FROM Hotels WHERE HotelID = ?";
				con = DatabaseConnection.getConnection();
				queryStatement = con.prepareStatement(query);
				queryStatement.setString(1, IdText.getText());
			} else if (!nameField.getText().isEmpty() && IdText.getText().isEmpty()) {

				String query = "SELECT * FROM Hotels WHERE Name LIKE ?";
				con = DatabaseConnection.getConnection();
				queryStatement = con.prepareStatement(query);
				queryStatement.setString(1, nameField.getText());

			} else {
				String query = "SELECT * FROM Hotels";
				con = DatabaseConnection.getConnection();
				queryStatement = con.prepareStatement(query);
			}
			// TODO Für die Benutzerinnen und Benutzer erklären, wie man etwas löschen kann,
			// und dass die ID -Suchleiste den Vorzug im Vergleich zu Nmaeleise hat

			ResultSet rs = queryStatement.executeQuery();

			while (rs.next()) {
				model.addRow(new Object[] { rs.getInt("HotelID"), rs.getString("Kategorie"), rs.getString("Name"),
						rs.getString("Besitzer"), rs.getString("Kontakt"), rs.getString("Adresse"),
						rs.getString("Stadt"), rs.getInt("PLZ"), rs.getInt("Telefon"), rs.getInt("ZimmerNr"),
						rs.getInt("BettenNr") });
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Ungültige Daten!");
		}

		table.setModel(model);
		table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		table.getColumnModel().getColumn(0);
		table.getColumnModel().getColumn(1);
		table.getColumnModel().getColumn(2);
		table.getColumnModel().getColumn(3);
		table.getColumnModel().getColumn(4);
		table.getColumnModel().getColumn(5);
		table.getColumnModel().getColumn(6);
		table.getColumnModel().getColumn(7);
		table.getColumnModel().getColumn(8);
		table.getColumnModel().getColumn(9);
		table.getColumnModel().getColumn(10);

	}

	public void hotelLöschen() throws Exception {

		
		Connection con = DatabaseConnection.getConnection();
		PreparedStatement statement = null;
		try {
			String query = "DELETE FROM Hotels WHERE HotelID = ? ";
			statement = con.prepareStatement(query);

			statement.setString(1, textDeleteID.getText());
			ResultSet rs = statement.executeQuery();
			
			if(rs.next()) {
				JOptionPane.showConfirmDialog(null, " Hotel mit der ID-Nummer " + textDeleteID.getText() +  "wird gelöscht");
			}
			else
			{
				JOptionPane.showConfirmDialog(null, "Es gibt kein Hotel mit der ID-Nummer!");
			}
			
			
		} catch (Exception e) {
		}
	}
	
	public void hotelsListe() throws SQLException {

		Connection con = DatabaseConnection.getConnection();

		DefaultTableModel model = new DefaultTableModel();
		model.addColumn("ID");
		model.addColumn("Kategorie");
		model.addColumn("Name");
		model.addColumn("Besitzer");
		model.addColumn("Kontakt");
		model.addColumn("Adresse");
		model.addColumn("Stadt");
		model.addColumn("PLZ");
		model.addColumn("Telefon");
		model.addColumn("Raumanzahl");
		model.addColumn("Bettenanzahl");

		String query = "SELECT * FROM Hotels";
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery(query);

		while (rs.next()) {
			model.addRow(new Object[] { rs.getInt("HotelID"), rs.getString("Kategorie"), rs.getString("Name"),
					rs.getString("Besitzer"), rs.getString("Kontakt"), rs.getString("Adresse"), rs.getString("Stadt"),
					rs.getInt("PLZ"), rs.getInt("Telefon"), rs.getInt("ZimmerNr"), rs.getInt("BettenNr") });
		}

		table.setModel(model);
		table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		table.getColumnModel().getColumn(0);
		table.getColumnModel().getColumn(1);
		table.getColumnModel().getColumn(2);
		table.getColumnModel().getColumn(3);
		table.getColumnModel().getColumn(4);
		table.getColumnModel().getColumn(5);
		table.getColumnModel().getColumn(6);
		table.getColumnModel().getColumn(7);
		table.getColumnModel().getColumn(8);
		table.getColumnModel().getColumn(9);
		table.getColumnModel().getColumn(10);

	}

	public void extrasAnzeigen() throws SQLException {

		Connection con = null;
		PreparedStatement queryStatement = null;

		DefaultTableModel model = new DefaultTableModel();
		model.addColumn("ID");
		model.addColumn("Kategorie");
		model.addColumn("Name");
		model.addColumn("Besitzer");
		model.addColumn("Kontakt");
		model.addColumn("Adresse");
		model.addColumn("Stadt");
		model.addColumn("PLZ");
		model.addColumn("Telefon");
		model.addColumn("Raumanzahl");
		model.addColumn("Bettenanzahl");

	
		try {
			
			if(!comboFamilie.getSelectedItem().equals("") && !comboFitness.getSelectedItem().equals("") && !comboSpa.getSelectedItem().equals("") && !comboHund.getSelectedItem().equals("")) {
				String query = "SELECT h.HotelID, h.Kategorie, h.Name, h.Besitzer, h.Kontakt, h.Adresse, h.Stadt, h.PLZ, h.Telefon, h.ZimmerNr, h.BettenNr "
						+ "FROM Hotels h INNER JOIN Extras e ON(h.HotelID=e.HotelID)"
						+ " WHERE e.familienfreundlich = ? AND e.fitness = ? AND e.spa = ? AND e.hundefreundlich = ? ";
				con = DatabaseConnection.getConnection();
				queryStatement = con.prepareStatement(query);
				queryStatement.setString(1, (String) comboFamilie.getSelectedItem());
				queryStatement.setString(2, (String) comboFitness.getSelectedItem());
				queryStatement.setString(3, (String) comboSpa.getSelectedItem());
				queryStatement.setString(4, (String) comboHund.getSelectedItem());
			}
			
			else if(comboFamilie.getSelectedItem().equals("JA")) {
				String query = "SELECT h.HotelID, h.Kategorie, h.Name, h.Besitzer, h.Kontakt, h.Adresse, h.Stadt, h.PLZ, h.Telefon, h.ZimmerNr, h.BettenNr "
						+ "FROM Hotels h INNER JOIN Extras e ON(h.HotelID=e.HotelID)"
						+ " WHERE e.familienfreundlich = ? ";
				con = DatabaseConnection.getConnection();
				queryStatement = con.prepareStatement(query);
				queryStatement.setString(1, (String) comboFamilie.getSelectedItem());
			}
			else if(comboSpa.getSelectedItem().equals("JA")) {
				String query = "SELECT h.HotelID, h.Kategorie, h.Name, h.Besitzer, h.Kontakt, h.Adresse, h.Stadt, h.PLZ, h.Telefon, h.ZimmerNr, h.BettenNr "
						+ "FROM Hotels h INNER JOIN Extras e ON(h.HotelID=e.HotelID)"
						+ " WHERE e.spa = ? ";
				con = DatabaseConnection.getConnection();
				queryStatement = con.prepareStatement(query);
				queryStatement.setString(1, (String) comboSpa.getSelectedItem());
			}
			else if(comboHund.getSelectedItem().equals("JA")) {
				String query = "SELECT h.HotelID, h.Kategorie, h.Name, h.Besitzer, h.Kontakt, h.Adresse, h.Stadt, h.PLZ, h.Telefon, h.ZimmerNr, h.BettenNr "
						+ "FROM Hotels h INNER JOIN Extras e ON(h.HotelID=e.HotelID)"
						+ " WHERE e.hundefreundlich = ? ";
				con = DatabaseConnection.getConnection();
				queryStatement = con.prepareStatement(query);
				queryStatement.setString(1, (String) comboHund.getSelectedItem());
			}
			else if(comboFitness.getSelectedItem().equals("JA")) {
				String query = "SELECT h.HotelID, h.Kategorie, h.Name, h.Besitzer, h.Kontakt, h.Adresse, h.Stadt, h.PLZ, h.Telefon, h.ZimmerNr, h.BettenNr "
						+ "FROM Hotels h INNER JOIN Extras e ON(h.HotelID=e.HotelID)"
						+ " WHERE e.fitness = ? ";
				con = DatabaseConnection.getConnection();
				queryStatement = con.prepareStatement(query);
				queryStatement.setString(1, (String) comboFitness.getSelectedItem());
			}	
			if(!comboFamilie.getSelectedItem().equals("") && !comboFitness.getSelectedItem().equals("") && !comboSpa.getSelectedItem().equals("") && !comboHund.getSelectedItem().equals("")) {
				String query = "SELECT h.HotelID, h.Kategorie, h.Name, h.Besitzer, h.Kontakt, h.Adresse, h.Stadt, h.PLZ, h.Telefon, h.ZimmerNr, h.BettenNr "
						+ "FROM Hotels h INNER JOIN Extras e ON(h.HotelID=e.HotelID)"
						+ " WHERE e.familienfreundlich = ? AND e.fitness = ? AND e.spa = ? AND e.hundefreundlich = ? ";
				con = DatabaseConnection.getConnection();
				queryStatement = con.prepareStatement(query);
				queryStatement.setString(1, (String) comboFamilie.getSelectedItem());
				queryStatement.setString(2, (String) comboFitness.getSelectedItem());
				queryStatement.setString(3, (String) comboSpa.getSelectedItem());
				queryStatement.setString(4, (String) comboHund.getSelectedItem());
			}
			else {
				
			}

			ResultSet rs = queryStatement.executeQuery();

			while (rs.next()) {
				model.addRow(new Object[] { rs.getInt("HotelID"), rs.getString("Kategorie"), rs.getString("Name"),
						rs.getString("Besitzer"), rs.getString("Kontakt"), rs.getString("Adresse"),
						rs.getString("Stadt"), rs.getInt("PLZ"), rs.getInt("Telefon"), rs.getInt("ZimmerNr"),
						rs.getInt("BettenNr") });
			}
			
			
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Ungültige Daten!");
		}

		table.setModel(model);
		table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		table.getColumnModel().getColumn(0);
		table.getColumnModel().getColumn(1);
		table.getColumnModel().getColumn(2);
		table.getColumnModel().getColumn(3);
		table.getColumnModel().getColumn(4);
		table.getColumnModel().getColumn(5);
		table.getColumnModel().getColumn(6);
		table.getColumnModel().getColumn(7);
		table.getColumnModel().getColumn(8);
		table.getColumnModel().getColumn(9);
		table.getColumnModel().getColumn(10);

	}
	
}
