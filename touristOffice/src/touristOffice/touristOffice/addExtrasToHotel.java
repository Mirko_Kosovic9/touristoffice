package touristOffice;

import java.awt.EventQueue;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JFrame;
import javax.swing.table.DefaultTableModel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Color;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;
import javax.swing.SwingConstants;

public class addExtrasToHotel {

	private JFrame frmExtrasHinzufgen;
	private JTable table;
	private JTextField idText;
	private JComboBox comboBoxFitness;
	private JComboBox comboBoxSpa;
	private JComboBox comboBoxFamilie;
	private JComboBox comboBoxHund;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					addExtrasToHotel window = new addExtrasToHotel();
					window.frmExtrasHinzufgen.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public addExtrasToHotel() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmExtrasHinzufgen = new JFrame();
		frmExtrasHinzufgen.setTitle("Extras hinzufügen");
		frmExtrasHinzufgen.setBounds(100, 100, 783, 328);
		frmExtrasHinzufgen.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmExtrasHinzufgen.getContentPane().setLayout(null);
		
		idText = new JTextField();
		idText.setBounds(304, 31, 112, 27);
		frmExtrasHinzufgen.getContentPane().add(idText);
		idText.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("In welches Hotel werden Extras hinzugefügt?");
		lblNewLabel.setBounds(10, 31, 284, 27);
		frmExtrasHinzufgen.getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Bitte ID eingeben!");
		lblNewLabel_1.setForeground(Color.RED);
		lblNewLabel_1.setBounds(304, 8, 112, 18);
		frmExtrasHinzufgen.getContentPane().add(lblNewLabel_1);
		
		JButton btAnzeigen = new JButton("Hoteldaten anzeigen");
		btAnzeigen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					hotelsWithExtras();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btAnzeigen.setBounds(440, 31, 167, 27);
		frmExtrasHinzufgen.getContentPane().add(btAnzeigen);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 83, 759, 56);
		frmExtrasHinzufgen.getContentPane().add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		
		JLabel lblNewLabel_2 = new JLabel("Neue Extras auswählen: ");
		lblNewLabel_2.setBounds(23, 149, 167, 43);
		frmExtrasHinzufgen.getContentPane().add(lblNewLabel_2);
		
		 comboBoxHund = new JComboBox();
		 comboBoxHund.addItem("JA");
		 comboBoxHund.addItem("NEIN");
		comboBoxHund.setBounds(214, 194, 80, 21);
		frmExtrasHinzufgen.getContentPane().add(comboBoxHund);
		
		 comboBoxFamilie = new JComboBox();
		 comboBoxFamilie.addItem("JA");
		 comboBoxFamilie.addItem("NEIN");
		comboBoxFamilie.setBounds(352, 194, 80, 21);
		frmExtrasHinzufgen.getContentPane().add(comboBoxFamilie);
		
		 comboBoxSpa = new JComboBox();
		 comboBoxSpa.addItem("JA");
		 comboBoxSpa.addItem("NEIN");
		comboBoxSpa.setBounds(489, 194, 80, 21);
		frmExtrasHinzufgen.getContentPane().add(comboBoxSpa);
		
		 comboBoxFitness = new JComboBox();
		 comboBoxFitness.addItem("JA");
		 comboBoxFitness.addItem("NEIN");
		comboBoxFitness.setBounds(621, 194, 80, 21);
		frmExtrasHinzufgen.getContentPane().add(comboBoxFitness);
		
		JLabel lblNewLabel_3 = new JLabel("Hundefreundlich");
		lblNewLabel_3.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_3.setBounds(200, 149, 108, 28);
		frmExtrasHinzufgen.getContentPane().add(lblNewLabel_3);
		
		JLabel lblNewLabel_3_1 = new JLabel("Familienfreundlich");
		lblNewLabel_3_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_3_1.setBounds(341, 149, 108, 28);
		frmExtrasHinzufgen.getContentPane().add(lblNewLabel_3_1);
		
		JLabel lblNewLabel_3_1_1 = new JLabel("Spa");
		lblNewLabel_3_1_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_3_1_1.setBounds(478, 149, 108, 28);
		frmExtrasHinzufgen.getContentPane().add(lblNewLabel_3_1_1);
		
		JLabel lblNewLabel_3_1_2 = new JLabel("Fitness");
		lblNewLabel_3_1_2.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_3_1_2.setBounds(605, 149, 108, 28);
		frmExtrasHinzufgen.getContentPane().add(lblNewLabel_3_1_2);
		
		JButton btAddExtras = new JButton("Extras hinzufügen");
		btAddExtras.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					addExtras();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btAddExtras.setBounds(317, 241, 191, 40);
		frmExtrasHinzufgen.getContentPane().add(btAddExtras);
	}
	
	public void hotelsWithExtras() throws SQLException {
		
		
		Connection con = DatabaseConnection.getConnection();
		PreparedStatement queryStatement = null;
		
		DefaultTableModel model = new DefaultTableModel();
		model.addColumn("HotelID");
		model.addColumn("Name");
		model.addColumn("Hundefreundlich");
		model.addColumn("Familienfreundlich");
		model.addColumn("Spa");
		model.addColumn("Fitness");

		
		String query = "SELECT h.HotelID AS HotelID, h.Name AS Name, (ISNULL(e.hundefreundlich,'Unbekannt')) as hundefreundlich, (ISNULL(e.spa,'Unbekannt')) as spa, (ISNULL(e.fitness,'Unbekannt')) as fitness,"
		+ " (ISNULL(e.familienfreundlich,'Unbekannt')) as familienfreundlich FROM Hotels h FULL JOIN Extras e ON(h.HotelID=e.HotelID)" 
		+ " WHERE h.HotelID = ?";
		
		queryStatement = con.prepareStatement(query);
		
		queryStatement.setString(1, idText.getText());
		
		ResultSet rs = queryStatement.executeQuery();
		
		while(rs.next()) {
			model.addRow(new Object[] { rs.getInt("HotelID"), rs.getString("Name"), rs.getString("hundefreundlich"), rs.getString("familienfreundlich"),
					rs.getString("spa"), rs.getString("fitness") });
		}
		
		table.setModel(model);
		table.getColumnModel().getColumn(0).setPreferredWidth(15);
		table.getColumnModel().getColumn(1);
		table.getColumnModel().getColumn(2);
		table.getColumnModel().getColumn(3);
		table.getColumnModel().getColumn(4);
		table.getColumnModel().getColumn(5);
		
	}
	
	public void addExtras() throws SQLException{
		
		Connection con = DatabaseConnection.getConnection();
		PreparedStatement statement = null;
		
		String query = "INSERT INTO  Extras VALUES  (?,?,?,?,?)";
		statement = con.prepareStatement(query);
		
		statement.setString(1, idText.getText());
		statement.setString(2, (String) comboBoxHund.getSelectedItem());
		statement.setString(3, (String) comboBoxFamilie.getSelectedItem());
		statement.setString(4, (String) comboBoxSpa.getSelectedItem());
		statement.setString(5, (String) comboBoxFitness.getSelectedItem());
		
		int rs = statement.executeUpdate();
		
		if(rs==1) {
			JOptionPane.showMessageDialog(null, "Erfolgreich hinzugefügt!");
		}
		

	}
}
