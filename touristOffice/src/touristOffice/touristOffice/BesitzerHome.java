package touristOffice;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;

public class BesitzerHome {

	private JFrame frmHome;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BesitzerHome window = new BesitzerHome();
					window.frmHome.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public BesitzerHome() {
		initialize();
	}

	private void initialize() {

		frmHome = new JFrame();
		frmHome.setFont(new Font("DialogInput", Font.BOLD, 17));
		frmHome.setTitle("Home");
		frmHome.setBounds(100, 100, 671, 411);
		frmHome.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmHome.getContentPane().setLayout(null);

		JButton btnAnlegen = new JButton("Hotel Anlegen");
		btnAnlegen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					HotelAnlegen anlegen = new HotelAnlegen();
					anlegen.main(null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		btnAnlegen.setFont(new Font("Dubai", Font.BOLD, 15));
		btnAnlegen.setBounds(10, 23, 191, 69);
		frmHome.getContentPane().add(btnAnlegen);

		JButton btnHListe = new JButton("Hotel Liste");
		btnHListe.setFont(new Font("Dubai", Font.BOLD, 15));
		btnHListe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					HotelListe liste = new HotelListe();
					liste.main(null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		btnHListe.setBounds(232, 102, 191, 69);
		frmHome.getContentPane().add(btnHListe);

		JButton btnBelegung = new JButton("Alle Betriebe");
		btnBelegung.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
				AlleBetriebe betriebe = new AlleBetriebe();
					AlleBetriebe.main(null);
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		});
		btnBelegung.setFont(new Font("Dubai", Font.BOLD, 15));
		btnBelegung.setBounds(232, 23, 191, 69);
		frmHome.getContentPane().add(btnBelegung);

		JButton btnStatistik = new JButton("Statistik Exportieren");
		btnStatistik.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				NewStatistic Statistik;
				
					NewStatistic s = new NewStatistic();
					s.main(null);
				
			}
			}
		);
		btnStatistik.setFont(new Font("Dubai", Font.BOLD, 15));
		btnStatistik.setBounds(232, 181, 191, 69);
		frmHome.getContentPane().add(btnStatistik);
		
		JButton benutzerRegistration_1 = new JButton("Benutzer Registrieren");
		benutzerRegistration_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				BenutzerRegisterForm.main(null);
			}
		});
		benutzerRegistration_1.setFont(new Font("Dubai", Font.BOLD, 15));
		benutzerRegistration_1.setBounds(456, 23, 191, 69);
		frmHome.getContentPane().add(benutzerRegistration_1);
		
		JButton benutzerRegistration_2 = new JButton("Besitzer Registrieren");
		benutzerRegistration_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				BesitzerRegisterForm.main(null);
			}
		});
		benutzerRegistration_2.setFont(new Font("Dubai", Font.BOLD, 15));
		benutzerRegistration_2.setBounds(456, 102, 191, 69);
		frmHome.getContentPane().add(benutzerRegistration_2);
		
		JButton btnBearbeiten = new JButton("Hotel bearbeiten");
		btnBearbeiten.setFont(new Font("Dubai", Font.BOLD, 15));
		btnBearbeiten.setBounds(10, 102, 191, 69);
		frmHome.getContentPane().add(btnBearbeiten);
		
		
		JButton btnLoeschen = new JButton("Hotel löschen");
		btnLoeschen.setFont(new Font("Dubai", Font.BOLD, 15));
		btnLoeschen.setBounds(10, 181, 191, 69);
		frmHome.getContentPane().add(btnLoeschen);
		
		JLabel iconLabel = new JLabel("");
		iconLabel.setBounds(21, 270, 165, 94);
		frmHome.getContentPane().add(iconLabel);
		
		JButton btnHilfe = new JButton("Hilfe");
		btnHilfe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			}
		});
		btnHilfe.setFont(new Font("Dubai", Font.BOLD, 15));
		btnHilfe.setBounds(456, 181, 191, 69);
		frmHome.getContentPane().add(btnHilfe);
	}

	protected static void dispose() {
	}
}
