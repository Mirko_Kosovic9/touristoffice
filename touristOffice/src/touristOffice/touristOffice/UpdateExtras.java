package touristOffice;

import java.awt.EventQueue;
import java.awt.ScrollPane;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.SystemColor;
import javax.swing.JTextPane;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class UpdateExtras {

	private JFrame frmExtrasndern;
	private JTable tableExtras;
	private JScrollPane paneScroll;
	private JComboBox comboHund;
	private JComboBox comboFamilie;
	private JComboBox comboSpa;
	private JComboBox comboFitness;
	private JTextPane textID;
	private JButton btnNewButton_1;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UpdateExtras window = new UpdateExtras();
					window.frmExtrasndern.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public UpdateExtras() throws SQLException {
		initialize();
		showExtras();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmExtrasndern = new JFrame();
		frmExtrasndern.setTitle("Extras ändern");
		frmExtrasndern.setBounds(100, 100, 749, 376);
		frmExtrasndern.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmExtrasndern.getContentPane().setLayout(null);
		
		paneScroll = new JScrollPane();
		paneScroll.setBounds(10, 39, 715, 222);
		frmExtrasndern.getContentPane().add(paneScroll);
		tableExtras = new JTable();
		paneScroll.setViewportView(tableExtras);
		
		textID = new JTextPane();
		textID.setBounds(10, 271, 97, 26);
		frmExtrasndern.getContentPane().add(textID);
		
		 comboHund = new JComboBox();
		comboHund.addItem("JA");
		comboHund.addItem("NEIN");
		comboHund.setBounds(117, 271, 87, 26);
		frmExtrasndern.getContentPane().add(comboHund);
		
		 comboFamilie = new JComboBox();
		comboFamilie.addItem("JA");
		comboFamilie.addItem("NEIN");
		comboFamilie.setBounds(239, 271, 87, 26);
		frmExtrasndern.getContentPane().add(comboFamilie);
		
		 comboSpa = new JComboBox();
		comboSpa.addItem("JA");
		comboSpa.addItem("NEIN");
		comboSpa.setBounds(344, 271, 87, 26);
		frmExtrasndern.getContentPane().add(comboSpa);
		
		 comboFitness = new JComboBox();
		comboFitness.addItem("JA");
		comboFitness.addItem("NEIN");
		comboFitness.setBounds(440, 271, 81, 26);
		frmExtrasndern.getContentPane().add(comboFitness);
		
		JLabel lblNewLabel = new JLabel("Hotel ID");
		lblNewLabel.setBounds(10, 307, 97, 22);
		frmExtrasndern.getContentPane().add(lblNewLabel);
		
		JLabel lblHundefreundlich = new JLabel("Hundefreundlich");
		lblHundefreundlich.setBounds(103, 307, 97, 22);
		frmExtrasndern.getContentPane().add(lblHundefreundlich);
		
		JLabel lblFamilienfreundlich = new JLabel("Familienfreundlich");
		lblFamilienfreundlich.setBounds(228, 307, 106, 22);
		frmExtrasndern.getContentPane().add(lblFamilienfreundlich);
		
		JLabel lblSpa = new JLabel("Spa");
		lblSpa.setBounds(344, 307, 62, 22);
		frmExtrasndern.getContentPane().add(lblSpa);
		
		JLabel lblFitness = new JLabel("Fitness");
		lblFitness.setBounds(440, 307, 69, 22);
		frmExtrasndern.getContentPane().add(lblFitness);
		
		JButton btnNewButton = new JButton("Änderung speichern");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					update();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnNewButton.setBounds(545, 271, 163, 44);
		frmExtrasndern.getContentPane().add(btnNewButton);
		
		btnNewButton_1 = new JButton("Aktualisieren");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					showExtras();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
//					e1.printStackTrace();
				}
			}
		});
		btnNewButton_1.setBounds(603, 8, 122, 21);
		frmExtrasndern.getContentPane().add(btnNewButton_1);

	}
	
	public void showExtras() throws SQLException {
	Connection con = null;
	PreparedStatement queryStatement = null;
	
	DefaultTableModel model = new DefaultTableModel();
	model.addColumn("HotelID");
	model.addColumn("Name");
	model.addColumn("Hundefreundlich");
	model.addColumn("Familienfreundlich");
	model.addColumn("Spa");
	model.addColumn("Fitness");
	
	con = DatabaseConnection.getConnection();
	
	String extrasQuery = "SELECT e.HotelID AS 'HotelID', h.Name AS 'Name', e.hundefreundlich AS 'Hundefreundlich', e.familienfreundlich AS 'Familienfreundlich',"
			+ " e.spa AS 'Spa', e.fitness AS 'Fitness' FROM Extras e INNER join Hotels h ON (h.HotelID=e.HotelID)";
	queryStatement = con.prepareStatement(extrasQuery);
	
	ResultSet rs = queryStatement.executeQuery();
	
	while(rs.next()) {
		model.addRow(new Object[] { rs.getInt("HotelID"), rs.getString("Name"), rs.getString("hundefreundlich"), rs.getString("familienfreundlich"),
				rs.getString("spa"), rs.getString("fitness") });
	}

	tableExtras.setModel(model);
	tableExtras.getColumnModel().getColumn(0);
	tableExtras.getColumnModel().getColumn(1);
	tableExtras.getColumnModel().getColumn(2);
	tableExtras.getColumnModel().getColumn(3);
	tableExtras.getColumnModel().getColumn(4);
	tableExtras.getColumnModel().getColumn(5);
	}
	
	public void update() throws SQLException {
		
		DefaultTableModel model = new DefaultTableModel();
		model.addColumn("HotelID");
		model.addColumn("Name");
		model.addColumn("Hundefreundlich");
		model.addColumn("Familienfreundlich");
		model.addColumn("Spa");
		model.addColumn("Fitness");
		
		
		Connection con = null;
		PreparedStatement queryStatement = null;

		
		con = DatabaseConnection.getConnection();
		String extrasQuery = "UPDATE Extras SET hundefreundlich = ?, familienfreundlich = ?, spa = ?, Fitness = ? WHERE HotelID = ?";
		
		queryStatement = con.prepareStatement(extrasQuery);
		
		queryStatement.setString(1, (String) comboHund.getSelectedItem());
		queryStatement.setString(2, (String) comboFamilie.getSelectedItem());
		queryStatement.setString(3, (String) comboSpa.getSelectedItem());
		queryStatement.setString(4, (String) comboFitness.getSelectedItem());
		queryStatement.setString(5, textID.getText());
		
		boolean rs = queryStatement.execute();
	}
}
