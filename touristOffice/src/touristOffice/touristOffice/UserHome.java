package touristOffice;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.plaf.basic.BasicInternalFrameTitlePane.CloseAction;
import javax.swing.JMenu;
import java.awt.Color;
import java.awt.Desktop.Action;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class UserHome {

	private JFrame frmHome;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UserHome window = new UserHome();
					window.frmHome.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public UserHome() {
		initialize();
	}

	private void initialize() {

		frmHome = new JFrame();
		frmHome.setFont(new Font("DialogInput", Font.BOLD, 17));
		frmHome.setTitle("Home");
		frmHome.setBounds(100, 100, 671, 411);
		frmHome.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmHome.getContentPane().setLayout(null);

		JButton btnAnlegen = new JButton("Hotel Anlegen");
		btnAnlegen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					HotelAnlegen anlegen = new HotelAnlegen();
					anlegen.main(null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		btnAnlegen.setFont(new Font("Dubai", Font.BOLD, 15));
		btnAnlegen.setBounds(10, 23, 191, 69);
		frmHome.getContentPane().add(btnAnlegen);

		JButton btnHListe = new JButton("Hotel Liste");
		btnHListe.setFont(new Font("Dubai", Font.BOLD, 15));
		btnHListe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					HotelListe liste = new HotelListe();
					liste.main(null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		btnHListe.setBounds(456, 23, 191, 69);
		frmHome.getContentPane().add(btnHListe);

		JButton btnBelegung = new JButton("Alle Betriebe");
		btnBelegung.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
				AlleBetriebe betriebe = new AlleBetriebe();
					AlleBetriebe.main(null);
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		});
		btnBelegung.setFont(new Font("Dubai", Font.BOLD, 15));
		btnBelegung.setBounds(232, 23, 191, 69);
		frmHome.getContentPane().add(btnBelegung);
		
		JButton statistikBtn = new JButton("Statistik ansehen");
		statistikBtn.setFont(new Font("Dubai", Font.BOLD, 15));
		statistikBtn.setBounds(232, 122, 191, 69);
		frmHome.getContentPane().add(statistikBtn);
		
		JMenuBar menuBar = new JMenuBar();
		frmHome.setJMenuBar(menuBar);
		
		JMenu mnNewMenu = new JMenu("Hilfe");
		mnNewMenu.setBackground(new Color(204, 204, 255));
		menuBar.add(mnNewMenu);
		
		JMenuItem helpItem = new JMenuItem("User-Handbuch");
		helpItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				HandbookUser hb = new HandbookUser();
				hb.main(null);
			}
		});
		mnNewMenu.add(helpItem);
		
		JMenuItem mntmNewMenuItem = new JMenuItem("Technische Support");
		mntmNewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				BenutzerSupport.main(null);
			}
		});
		mnNewMenu.add(mntmNewMenuItem);
		
		JMenuItem abmeldenBtn = new JMenuItem("Abmelden");
		abmeldenBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				LogIn login = new LogIn();
				frmHome.dispose();
				login.main(null);
			}

		});
		mnNewMenu.add(abmeldenBtn);
	}
}
