package touristOffice;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
//import com.itextpdf.text.Document;
//import com.itextpdf.text.DocumentException;
//import com.itextpdf.text.pdf.PdfPTable;
//import com.itextpdf.text.pdf.PdfWriter;
//import com.sun.istack.logging.Logger;
import javax.swing.text.Document;
import javax.swing.JTextField;
import javax.swing.JFileChooser;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;

public class NewStatistic extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField hotel_id;
	protected Object frmListeAllerHotels;
	private JScrollPane scrollPane;
	private JTable table_stat;
	private JTable stat_table;
	private TableRowSorter<DefaultTableModel> sorter;
	private DefaultTableModel model;
	// final JComboBox<String> belegung_monat = new JComboBox<>(new String[]
	// {/*"2014","2015","2016","2017","2018","2019"*/});
	private JComboBox belegung_jahr = new JComboBox();
	JComboBox belegung_monat = new JComboBox();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					NewStatistic window = new NewStatistic();
					window.hotelsAnzeigen();

					window.setVisible(true);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public void hotelsAnzeigen() throws SQLException {

		Connection con = DatabaseConnection.getConnection();

		DefaultTableModel model = new DefaultTableModel();
		model.addColumn("ID");
		model.addColumn("Hotelname");
		model.addColumn("Betten");
		model.addColumn("BettenBelegt");
		model.addColumn("Zimmer");
		model.addColumn("ZimmerBelegt");

		/*
		 * model.addColumn("Besitzer"); model.addColumn("Kontakt");
		 * model.addColumn("Adresse"); model.addColumn("Stadt"); model.addColumn("PLZ");
		 * model.addColumn("Telefon"); model.addColumn("Raumanzahl");
		 * model.addColumn("Bettenanzahl");
		 */
		try {
			String query = "SELECT b.HotelID, h.Name AS 'Hotelname', b.Betten, b.BettenBelegt, b.Zimmer, b.ZimmerBelegt"
					+ "FROM Belegung b" + "JOIN Hotels h ON(h.HotelID = b.HotelID)"
					+ "WHERE b.HotelID = ? AND Jahr = ?";
			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery(query);

			while (rs.next()) {
				model.addRow(new Object[] { rs.getInt("HotelID"), rs.getString("Hotelname"), rs.getInt("Betten"),
						rs.getInt("BettenBelegt"), rs.getInt("Zimmer"), rs.getInt("ZimmerBelegt") });
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Problem beim Laden!");
		}

		stat_table.setModel(model);
		stat_table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		stat_table.getColumnModel().getColumn(0);
		stat_table.getColumnModel().getColumn(1);
		stat_table.getColumnModel().getColumn(2);
		stat_table.getColumnModel().getColumn(3);
		stat_table.getColumnModel().getColumn(4);
		stat_table.getColumnModel().getColumn(5);
		/*
		 * stat_table.getColumnModel().getColumn(3);
		 * stat_table.getColumnModel().getColumn(4);
		 * stat_table.getColumnModel().getColumn(5); //
		 * stat_table.getColumnModel().getColumn(6);
		 * stat_table.getColumnModel().getColumn(7);
		 * stat_table.getColumnModel().getColumn(8);
		 * stat_table.getColumnModel().getColumn(9);
		 * stat_table.getColumnModel().getColumn(10);
		 */
	}

	public void hotelAnzeigen() throws SQLException {

		Connection con = null;
		PreparedStatement queryStatement = null;

		DefaultTableModel model = new DefaultTableModel();
		model.addColumn("ID");
		model.addColumn("Hotelname");
		model.addColumn("Betten");
		model.addColumn("BettenBelegt");
		model.addColumn("Zimmer");
		model.addColumn("ZimmerBelegt");
		/*
		 * model.addColumn("Besitzer"); model.addColumn("Kontakt");
		 * model.addColumn("Adresse"); model.addColumn("Stadt"); model.addColumn("PLZ");
		 * model.addColumn("Telefon"); model.addColumn("Raumanzahl");
		 * model.addColumn("Bettenanzahl");
		 */

		try {

			if (belegung_monat.getSelectedItem().equals("") && belegung_jahr.getSelectedItem().equals("")
					&& hotel_id.getText().equals("")) {
				System.out.println("ALLES IST Leer");
			}

			// FUNKTIONIERT
			if (!hotel_id.getText().isEmpty()) {
				String hotelId = "SELECT b.HotelID, h.Name AS 'Hotelname', b.Betten, b.BettenBelegt, b.Zimmer, b.ZimmerBelegt FROM Belegung b JOIN Hotels h ON(h.HotelID = b.HotelID) WHERE b.HotelID = ? AND b.Jahr = ? AND b.Monat = ?";
				con = DatabaseConnection.getConnection();
				queryStatement = con.prepareStatement(hotelId);
				queryStatement.setString(1, hotel_id.getText());
				queryStatement.setString(2, (String) belegung_jahr.getSelectedItem());
				queryStatement.setString(3, (String) belegung_monat.getSelectedItem());

				// FUNKTIONIERT
			} else if (hotel_id.getText().isEmpty()) {
				String query2 = "SELECT b.HotelID, h.Name AS 'Hotelname', b.Betten, b.BettenBelegt, b.Zimmer, b.ZimmerBelegt FROM Belegung b JOIN Hotels h ON(h.HotelID = b.HotelID) WHERE b.Jahr = ? AND b.Monat = ?";
				con = DatabaseConnection.getConnection();
				queryStatement = con.prepareStatement(query2);
				queryStatement.setString(1, (String) belegung_jahr.getSelectedItem());
				queryStatement.setString(2, (String) belegung_monat.getSelectedItem());

			}

			else if (!hotel_id.getText().isEmpty()) {
				String query3 = "SELECT b.HotelID, h.Name AS 'Hotelname', b.Betten, b.BettenBelegt, b.Zimmer, b.ZimmerBelegt FROM Belegung b JOIN Hotels h ON(h.HotelID = b.HotelID) WHERE b.HotelID = ?";
				con = DatabaseConnection.getConnection();
				queryStatement = con.prepareStatement(query3);
				queryStatement.setString(1, (String) belegung_jahr.getSelectedItem());
				// queryStatement.setString(2, (String)belegung_monat.getSelectedItem());
			}

			// FUNCTIONING CODE!!!
			else if (!hotel_id.getText().isEmpty() && !belegung_jahr.getToolTipText().isEmpty()
					&& !belegung_monat.getToolTipText().isEmpty()) {
				String monat = "SELECT b.HotelID, h.Name AS 'Hotelname', b.Jahr, b.Monat From Belegung b join Hotels h ON(b.HotelID=h.HotelID) WHERE b.HotelID = ? AND b.Jahr = ? AND b.Monat = ? ";
				con = DatabaseConnection.getConnection();
				queryStatement = con.prepareStatement(monat);
				queryStatement.setString(1, hotel_id.getText());
				queryStatement.setString(2, (String) belegung_jahr.getSelectedItem());
				queryStatement.setInt(3, (int) belegung_monat.getSelectedItem());
			}

			else if (!hotel_id.getText().isEmpty() && !belegung_jahr.getToolTipText().isEmpty()) {
				String monat = "SELECT b.HotelID, h.Name AS 'Hotelname', b.Betten, b.BettenBelegt, b.Zimmer, b.ZimmerBelegt FROM Belegung b JOIN Hotels h ON(h.HotelID = b.HotelID) WHERE b.HotelID = ? AND b.Jahr = ?";
				con = DatabaseConnection.getConnection();
				queryStatement = con.prepareStatement(monat);
				queryStatement.setString(1, hotel_id.getText());
				queryStatement.setString(2, (String) belegung_jahr.getSelectedItem());

			} else if (hotel_id.getText().isEmpty()) {
				String query2 = "SELECT b.HotelID, h.Name AS 'Hotelname', b.Jahr, b.Monat From Belegung b join Hotels h ON(b.HotelID=h.HotelID) WHERE  Jahr LIKE ? ";
				con = DatabaseConnection.getConnection();
				queryStatement = con.prepareStatement(query2);
				queryStatement.setString(1, (String) belegung_jahr.getSelectedItem());

			} else {

				String query = "SELECT * FROM Belegung ORDER BY HotelID";
				con = DatabaseConnection.getConnection();
				queryStatement = con.prepareStatement(query);
			}

			ResultSet rs = queryStatement.executeQuery();

			while (rs.next()) {
				model.addRow(new Object[] { rs.getInt("HotelID"), rs.getString("Hotelname"), rs.getInt("Betten"),
						rs.getInt("BettenBelegt"), rs.getInt("Zimmer"), rs.getInt("ZimmerBelegt") });
			}
		} catch (Exception e) {
			// JOptionPane.showMessageDialog(null, "Problem beim Laden!");
		}

		stat_table.setModel(model);
		stat_table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		stat_table.getColumnModel().getColumn(0);
		stat_table.getColumnModel().getColumn(1);
		stat_table.getColumnModel().getColumn(2);
		stat_table.getColumnModel().getColumn(3);
		stat_table.getColumnModel().getColumn(4);
		stat_table.getColumnModel().getColumn(5);
	}
}
	/*
	 * stat_table.getColumnModel().getColumn(3);
	 * stat_table.getColumnModel().getColumn(4);
	 * stat_table.getColumnModel().getColumn(5);
	 * stat_table.getColumnModel().getColumn(6);
	 * stat_table.getColumnModel().getColumn(7);
	 * stat_table.getColumnModel().getColumn(8);
	 * stat_table.getColumnModel().getColumn(9);
	 * stat_table.getColumnModel().getColumn(10);
	 * 
	 */

	/**
	 * Create the frame.
	 */
//	public NewStatistic() {
//		setTitle("Statistik Exportieren");
//		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
//		setBounds(100, 100, 865, 437);
//		contentPane = new JPanel();
//		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
//		setContentPane(contentPane);
//		contentPane.setLayout(null);
//
//		JLabel lblNewLabel = new JLabel("Hotel ID");
//		lblNewLabel.setBounds(10, 21, 46, 14);
//		contentPane.add(lblNewLabel);
//
//		hotel_id = new JTextField();
//		hotel_id.setBounds(66, 18, 86, 20);
//		contentPane.add(hotel_id);
//		hotel_id.setColumns(10);
//
//		JLabel lblBelegungMonat = new JLabel("Belegung Jahr");
//		lblBelegungMonat.setBounds(311, 21, 86, 14);
//		contentPane.add(lblBelegungMonat);
//
//		JLabel lblBelegungJahr = new JLabel("Belegung Monat");
//		lblBelegungJahr.setBounds(640, 21, 103, 14);
//		contentPane.add(lblBelegungJahr);
//
//		JScrollPane scrollPane_1 = new JScrollPane();
//		scrollPane_1.setBounds(10, 123, 829, 193);
//		contentPane.add(scrollPane_1);
//
//		stat_table = new JTable();
//		scrollPane_1.setViewportView(stat_table);
//
//		JButton hotels_anzeigen = new JButton("Hotels Anzeigen");
//		hotels_anzeigen.addActionListener(new ActionListener() {
//			public void actionPerformed(ActionEvent e) {
//				try {
//					hotelAnzeigen();
//				} catch (Exception e1) {
//					e1.printStackTrace();
//				}
//			}
//		});
//
//		hotels_anzeigen.addKeyListener(new KeyAdapter() {
//			public void keyPressed(KeyEvent e) {
//			}
//		});
//
//		JButton pdfExport = new JButton("Zum PDF exportieren");
//		pdfExport.setBounds(10, 360, 182, 31);
//		contentPane.add(pdfExport);
//		pdfExport.addActionListener(new ActionListener() {
//			public void actionPerformed(ActionEvent e) {
//
//				String path = "";
//				JFileChooser j = new JFileChooser();
//				j.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
//				int x = j.showSaveDialog(NewStatistic.this);
//				if (x == JFileChooser.APPROVE_OPTION) {
//					path = j.getSelectedFile().getPath();
//				}
//
//				Document doc = new Document();
//
//				try {
//					PdfWriter.getInstance(doc, new FileOutputStream(path + "Belegungen.pdf"));
//
//					doc.open();
//
//					PdfPTable tbl = new PdfPTable(6);
//
//					// adding header
//					tbl.addCell("ID");
//					tbl.addCell("Hotelname");
//					tbl.addCell("Betten");
//					tbl.addCell("BettenBelegt");
//					tbl.addCell("Zimmer");
//					tbl.addCell("ZimmerBelegt");
//
//					for (int i = 0; i < stat_table.getRowCount(); i++) {
//						String ID = stat_table.getValueAt(i, 0).toString();
//						String hotelname = stat_table.getValueAt(i, 1).toString();
//						String Betten = stat_table.getValueAt(i, 2).toString();
//						String BettenBelegt = stat_table.getValueAt(i, 3).toString();
//						String Zimmer = stat_table.getValueAt(i, 4).toString();
//						String ZimmerBelegt = stat_table.getValueAt(i, 5).toString();
//
//						tbl.addCell(ID);
//						tbl.addCell(hotelname);
//						tbl.addCell(Betten);
//						tbl.addCell(BettenBelegt);
//						tbl.addCell(Zimmer);
//						tbl.addCell(ZimmerBelegt);
//
//						doc.add(tbl);
//
//					}
//				} catch (FileNotFoundException e2) {
//					// TODO Auto-generated catch block
////						Logger.getLogger(NewStatistic.class.getName(), null).log(Level.SEVERE, null, e2);
//					// e2.printStackTrace();
//				} catch (DocumentException e2) {
//					// TODO Auto-generated catch block
////						Logger.getLogger(NewStatistic.class.getName(), null).log(Level.SEVERE, null, e2);
//
//					// e2.printStackTrace();
//				}
//
//				doc.close();
//
//			}
//		});
//
//		hotels_anzeigen.setBounds(31, 54, 124, 23);
//		contentPane.add(hotels_anzeigen);
//
//		belegung_jahr.setBounds(401, 17, 78, 22);
//		contentPane.add(belegung_jahr);
//		belegung_jahr.addItem("");
//		belegung_jahr.addItem("2014");
//		belegung_jahr.addItem("2015");
//		belegung_jahr.addItem("2016");
//		belegung_jahr.addItem("2017");
//		belegung_jahr.addItem("2018");
//		belegung_jahr.addItem("2019");
//
//		belegung_monat.setBounds(736, 17, 95, 22);
//		contentPane.add(belegung_monat);
//		belegung_monat.addItem("");
//		belegung_monat.addItem("1");
//		belegung_monat.addItem("2");
//		belegung_monat.addItem("3");
//		belegung_monat.addItem("4");
//		belegung_monat.addItem("5");
//		belegung_monat.addItem("6");
//		belegung_monat.addItem("7");
//		belegung_monat.addItem("8");
//		belegung_monat.addItem("9");
//		belegung_monat.addItem("10");
//		belegung_monat.addItem("11");
//		belegung_monat.addItem("12");
//
//		JButton btnAbbrechen = new JButton("Abbrechen");
//		btnAbbrechen.addActionListener(new ActionListener() {
//			public void actionPerformed(ActionEvent e) {
//				dispose();
//			}
//		});
//		btnAbbrechen.setBounds(657, 360, 182, 31);
//		contentPane.add(btnAbbrechen);
//
//	}
//
//	/*
//	 * public TableModel populateTable(int qryType) throws SQLException {
//	 * DefaultTableModel tblModel = new DefaultTableModel() {
//	 * 
//	 * @Override public boolean isCellEditable(int row, int column) { return false;
//	 * } }; String qry =
//	 * "SELECT b.HotelID, h.Name AS 'Hotelname', b.Betten, b.BettenBelegt, b.Zimmer, b.ZimmerBelegt FROM Belegung b JOIN Hotels h ON(h.HotelID = b.HotelID) WHERE b.HotelID = ? AND b.Jahr = ? AND b.Monat = ?"
//	 * ; try(Connection con = DatabaseConnection.getConnection(); Statement stmt =
//	 * con.createStatement(); ResultSet rs = stmt.executeQuery(qry)) {
//	 * 
//	 * int numCols = rs.getMetaData().getColumnCount(); for(int col = 1; col <=
//	 * numCols; col++) { tblModel.addColumn(rs.getMetaData().getColumnLabel(col)); }
//	 * 
//	 * } return model;
//	 * 
//	 * }
//	 */
//
//	}
//}
